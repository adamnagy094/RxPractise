//
//  SubjectExamples.swift
//  rxbasics
//
//  Created by Nagy Ádám on 2018. 08. 07..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import RxSwift

enum SubjectsExampleError: Error {
    case irrelevantError
}

enum SubjectsExamples: String, CaseIterable, ExampleType {
    case publishSubject
    case behaviorSubject
    case replaySubject
    case variable

    var title: String {
        return rawValue.titleCased
    }

    var disposeHandler: DisposeHandler {
        return .disposeBag
    }

    var disposable: Disposable {
        switch self {
        case .publishSubject: return publishSubjectExample()
        case .behaviorSubject: return behaviorSubjectExample()
        case .replaySubject: return replaySubjectExample()
        case .variable: return variableExample()
        }
    }
    
    var color: UIColor {
        return UIColor.Difficulty.easy
    }
}

extension SubjectsExamples {
    private func publishSubjectExample() -> Disposable {
        let publishSubject = PublishSubject<String>()

        publishSubject.onNext("1st version")

        publishSubject.onNext("2nd version")

        publishSubject.onCompleted()

        let subscription2 = publishSubject.debug().subscribe()

        publishSubject.onNext("3rd version")

        return subscription2
    }

    private func replaySubjectExample() -> Disposable {
        let replaySubject = ReplaySubject<String>.create(bufferSize: 1)

        /*for idx in 0...5 {
            replaySubject.onNext("number \(idx)")
        }*/

        let subscription = replaySubject.debug().subscribe()

        //replaySubject.onNext("number 6")

        //replaySubject.onCompleted()

        return subscription
    }

    private func behaviorSubjectExample() -> Disposable {
        let behaviorSubject = BehaviorSubject<String>(value: "initial value")

        // request the current stored value in the subject
        var currentValue: String {
            do {
                return try behaviorSubject.value()
            } catch {
                return "no value"
            }
        }

        func debugCurrentValue() {
            print("value request -> current value behavior subject: \(currentValue)")
        }


        debugCurrentValue()
        behaviorSubject.onNext("1st value")

        let subscription = behaviorSubject.debug().subscribe()

        debugCurrentValue()
        behaviorSubject.onNext("2nd value")

        debugCurrentValue()
        //behaviorSubject.onError(SubjectsExampleError.irrelevantError)

        debugCurrentValue()
        behaviorSubject.onNext("3rd value")

        return subscription
    }

    private func variableExample() -> Disposable {
        let variable = Variable<String>("initial value")

        func debugCurrentValue() {
            print("value request -> current value behavior subject: \(variable.value)")
        }

        debugCurrentValue()

        variable.value = "1st value"

        let subscription = variable.asObservable().debug().subscribe()

        variable.value = "2nd value"

        variable.value = "3rd value"

        return subscription
    }
}

