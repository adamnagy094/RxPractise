//
//  ObservableExamples.swift
//  rxbasics
//
//  Created by Nagy Ádám on 2018. 08. 06..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import RxSwift

enum ExampleError: Error {
    case oddNumberError
    case nilError
    case urlSessionError
}

enum ObservableExamples: String, CaseIterable, ExampleType {
    case just
    case ofWithSingleElements
    case ofWithSingleArray
    case from
    case empty
    case never
    case deferred
    case createNumbers
    case createNumberArray
    case createExampleAsyncTaskWrapper
    case createUrlSessionWrapper

    var title: String {
        return rawValue.titleCased
    }

    var disposable: Disposable {
        switch self {
        case .just: return just()
        case .ofWithSingleElements: return ofWithSingleElements()
        case .ofWithSingleArray: return ofWithSingleArray()
        case .from: return from()
        case .empty: return empty()
        case .never: return never()
        case .deferred: return deferred()
        case .createNumbers: return createNumbers()
        case .createNumberArray: return createNumberArray()
        case .createExampleAsyncTaskWrapper: return createExampleAsyncTaskWrapper()
        case .createUrlSessionWrapper: return createURLSessionWrapper()
        }
    }

    var disposeHandler: DisposeHandler {
        return .disposeBag
    }
    
    var color: UIColor {
        return UIColor.Difficulty.easy
    }
}

extension ObservableExamples {
    private func just() -> Disposable {
        let numbers = Observable.just(1)
        return numbers.debug().subscribe()
    }

    private func ofWithSingleElements() -> Disposable {
        let numbers = Observable.of(1,2,3,4,5)
        return numbers.debug().subscribe()
    }

    private func ofWithSingleArray() -> Disposable {
        let numbers = Observable.of([1,2,3,4,5])
        return numbers.debug().subscribe()
    }

    private func from() -> Disposable {
        let numbers = Observable.from([1,2,3,4,5])
        return numbers.debug().subscribe()
    }

    private func empty() -> Disposable {
        let emptyObservation = Observable<Void>.empty()
        return emptyObservation
            .debug().subscribe()
    }

    private func never() -> Disposable {
        let neverObservation = Observable<Any>.never()
        return neverObservation
            .debug()
            .subscribe()
    }

    private func deferred() -> Disposable {
        let deferredObservable = Observable<String>.deferred {
            let coin = Coin.rand

            switch coin {
            case .head: return Observable.just("Head!!!!")
            case .tail: return Observable.just("Tail!!!!")
            }
        }

        return deferredObservable.debug().subscribe()
    }

    private func createObservable() -> Disposable {
        let observabe = Observable<String>.create { observer in
            observer.onNext("Hello World")
            observer.onCompleted()
            return Disposables.create()
        }

        return observabe.debug().subscribe()
    }

    private func createNumbers() -> Disposable {
        let numberObservable = Observable<[Int]>.create { observer in
            observer.onNext([1,2,3,4,5])
            observer.onCompleted()

            return Disposables.create()
        }

        return numberObservable.debug().subscribe()
    }

    private func createNumberArray() -> Disposable {
        // DIY
        return Observable<Void>.empty().debug().subscribe()
    }

    private func createExampleAsyncTaskWrapper() -> Disposable {
        let task = Observable<String>.create { observer in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                let randomNumber = Int(arc4random_uniform(10))

                if randomNumber % 2 == 0 {
                    observer.onNext("hello world")
                    observer.onCompleted()
                } else {
                    observer.onError(ExampleError.oddNumberError)
                }
            })

            return Disposables.create()
        }

        return task.debug().subscribe()
    }

    private func createURLSessionWrapper() -> Disposable {
        guard let url = URL(string: "https://www.google.com") else {
            return Observable<Data>.error(ExampleError.nilError).debug().subscribe()
        }

        let urlRequest = Observable<Data>.create { observer in
            let session = URLSession.shared.dataTask(with: url) { data, response, error in
                if let error = error {
                    observer.onError(error)
                }

                if let data = data {
                    observer.onNext(data)
                    observer.onCompleted()
                } else {
                    observer.onError(ExampleError.urlSessionError)
                }
            }

            session.resume()
            return Disposables.create()
        }

        return urlRequest.debug().subscribe()
    }
}
