//
//  OperatorExamples.swift
//  rxbasics
//
//  Created by Nagy Ádám on 2018. 08. 07..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import RxSwift

enum OperatorExamples: String, CaseIterable, ExampleType {
    case map
    case filter
    case merge
    case concat
    case zip
    case flatMap

    var title: String {
        return rawValue.titleCased
    }

    var disposable: Disposable {
        switch self {
        case .map: return mapExample()
        case .filter: return filterExample()
        case .merge: return mergeExample()
        case .concat: return concatExample()
        case .zip: return zipExample()
        case .flatMap: return flatMapExample()
        }
    }

    var disposeHandler: DisposeHandler {
        return .disposeBag
    }
    
    var color: UIColor {
        return UIColor.Difficulty.easy
    }
}

extension OperatorExamples {
    private func filterExample() -> Disposable {
        let numbers = Observable<Int>.of(1,2,3,4,5,6)

        return numbers
            .filter { number in
                return number % 2 == 0
            }
            .debug()
            .subscribe()
    }

    private func mapExample() -> Disposable {
        let numbers = Observable<Int>.of(1,2,3,4,5,6)

        func transform(number: Int) -> String {
            return "number: \(number)"
        }

        return numbers
            .map(transform)
            .map { stringValue in
                return Int(stringValue.components(separatedBy: " ").last!)!
            }
            .debug()
            .subscribe()
    }

    private func combineThem() -> Disposable {
        // DIY
        return Observable<Void>.empty().debug().subscribe()
    }

    private func mergeExample() -> Disposable {
        let numbers = Observable<Int>.of(1,2,3,4,5,6)
        let numbers2 = Observable<Int>.of(7,8,9,10)

        return Observable.merge(numbers, numbers2).debug().subscribe()
    }

    private func concatExample() -> Disposable {
        let numbers = Observable<Int>.of(1,2,3,4,5,6)
        let numbers2 = Observable<Int>.of(7,8,9,10)

        return numbers.concat(numbers2).debug().subscribe()
    }

    private func zipExample() -> Disposable {
        let numbers = Observable<Int>.of(1,2,3,4,5,6)
        let numbers2 = Observable<Int>.of(7,8,9,10)

        return Observable.zip(numbers, numbers2).debug().subscribe()
    }

    private func flatMapExample() -> Disposable {
        let asyncTask1 = Observable<String>.create { observer in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                observer.onNext("async task 1 finished")
                observer.onCompleted()
            })

            return Disposables.create()
        }

        let asyncTask2 = Observable<String>.create { observer in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                observer.onNext("async task 2 finished")
                observer.onCompleted()
            })
            return Disposables.create()
        }

        let asyncTask3 = Observable<String>.create { observer in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                observer.onNext("async task 3 finished")
                observer.onCompleted()
            })
            return Disposables.create()
        }

        return asyncTask1
            .debug("first", trimOutput: false)
            .flatMap { _ in return asyncTask2 }
            .debug("second", trimOutput: false)
            .flatMap { _ in return asyncTask3 }
            .debug("third", trimOutput: false)
            .subscribe()
    }
}
