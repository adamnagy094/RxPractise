//
//  DisposingAndTerminatingExamples.swift
//  rxbasics
//
//  Created by Nagy Ádám on 2018. 08. 07..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import RxSwift

enum DisposingAndTerminatingExamples: String, CaseIterable, ExampleType {
    case disposeManually
    case disposeByDisposeBag
    case missingDisposeWithMemoryLeak

    var title: String {
        return rawValue.titleCased
    }

    var disposable: Disposable {
        switch self {
        case .disposeManually: return finiteObservable()
        case .disposeByDisposeBag: return finiteObservable()
        case .missingDisposeWithMemoryLeak: return infiniteObservable()
        }
    }

    var disposeHandler: DisposeHandler {
        switch self {
        case .disposeByDisposeBag: return .disposeBag
        case .disposeManually: return .disposeManually
        case .missingDisposeWithMemoryLeak: return .doNotDispose
        }
    }
    
    var color: UIColor {
        return UIColor.Difficulty.easy
    }
}

extension DisposingAndTerminatingExamples {
    private func finiteObservable() -> Disposable {
        let observable = Observable<String>.create { observer in
            observer.onNext("")
            observer.onCompleted()

            return Disposables.create()
        }
        return observable.debug().subscribe()
    }

    private func infiniteObservable() -> Disposable {
        let observable = Observable<String>.create { observer in
            observer.onNext("")

            return Disposables.create()
        }
        return observable.debug().subscribe()
    }
}
