//
//  TraitsExamples.swift
//  rxbasics
//
//  Created by Nagy Ádám on 2018. 08. 07..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import RxSwift

enum TraitsExampleError: Error {
    case wrongURL
    case noDataOrError
}

enum TraitsExamples: String, CaseIterable, ExampleType {
    case single
    case completable
    case maybe

    var title: String {
        return rawValue.titleCased
    }

    var disposeHandler: DisposeHandler {
        return .disposeBag
    }

    var disposable: Disposable {
        switch self {
        case .single: return singleExample()
        case .completable: return completableExample()
        case .maybe: return maybeExample()
        }
    }
    
    var color: UIColor {
        return UIColor.Difficulty.easy
    }
}

extension TraitsExamples {
    private var networkRequest: Observable<Data> {
        return Observable<Data>.create { observer in
            guard let url = URL(string: "https://www.google.com") else {
                observer.onError(TraitsExampleError.wrongURL)
                return Disposables.create()
            }

            let session = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if let error = error {
                    observer.onError(error)
                }

                if let data = data {
                    observer.onNext(data)
                    observer.onCompleted()
                } else {
                    observer.onError(TraitsExampleError.noDataOrError)
                }
            })

            session.resume()
            return Disposables.create()
        }
    }

    private var randomCoin: Coin {
        return Coin.rand
    }

    private func singleExample() -> Disposable {
        let single = Single<String>.create { observer in
            observer(.success("hello world"))
            return Disposables.create()
        }

        return single.debug().subscribe()
    }

    private func completableExample() -> Disposable {
        let completable = Completable.create { observer in
            observer(.completed)
            return Disposables.create()
        }

        return completable.debug().subscribe()
    }

    private func maybeExample() -> Disposable {
        let maybe = Maybe<String>.create { observer in
            observer(.success("value"))

            return Disposables.create()
        }

        return maybe.debug().subscribe()
    }
}
