//
//  AdvancedOperators.swift
//  rxbasics
//
//  Created by Edem on 2018. 10. 03..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import RxSwift
import RxSwiftExt

enum AdvancedOperatorExamples: String, CaseIterable, ExampleType {
    case flatMap
    case flatMapLatest
    case concatMap
    case timeOut
    case delaySubscription
    case unwrap
    case skipUntil
    case take
    case takeWhile
    case takeUntil
    case distinctUntilChanged
    case throttle
    case toArray
    case enumerated
    case catchError
    case concat
    case merge
    case combineLatest
    case reduce
    case scan
    case buffer
    
    var title: String {
        return rawValue.titleCased
    }
    
    var disposable: Disposable {
        switch self {
        case .concatMap: return concatMapExample()
        case .flatMap: return flatMapExample()
        case .flatMapLatest: return flatMapLatestExample()
        case .timeOut: return timeOut()
        case .delaySubscription: return delaySubscription()
        case .unwrap: return unwrap()
        case .skipUntil: return skipUntil()
        case .take: return take()
        case .takeWhile: return takeWhile()
        case .takeUntil: return takeUntil()
        case .distinctUntilChanged: return distinctUntilChanged()
        case .throttle: return throttle()
        case .toArray: return toArray()
        case .enumerated: return enumerated()
        case .catchError: return catchError()
        case .concat: return concat()
        case .merge: return merge()
        case .combineLatest: return combineLatest()
        case .reduce: return reduce()
        case .scan: return scan()
        case .buffer: return buffer()
        }
    }
    
    var disposeHandler: DisposeHandler {
        return .disposeBag
    }
    
    var color: UIColor {
        return UIColor.Difficulty.medium
    }
}

extension AdvancedOperatorExamples {
    func flatMapExample() -> Disposable {
        let numbers: Observable<Int> = Observable
            .interval(0.5, scheduler: MainScheduler.instance)
            .take(2)
        
        
        let combinedObservable = numbers.flatMap { value in
            return Observable<Int>
                .interval(0.3, scheduler: MainScheduler.instance)
                .take(3)
                .map { innerValue in
                    print("Outer Value \(value) Inner Value \(innerValue)")
                }
        }
        
        return combinedObservable.debug().subscribe()
    }
    
    func flatMapLatestExample() -> Disposable {
        let numbers: Observable<Int> = Observable
            .interval(0.5, scheduler: MainScheduler.instance)
            .take(2)
        
        
        let combinedObservable = numbers.flatMapLatest { value in
            return Observable<Int>
                .interval(0.3, scheduler: MainScheduler.instance)
                .take(3)
                .map { innerValue in
                    print("Outer Value \(value) Inner Value \(innerValue)")
            }
        }
        
        return combinedObservable.debug().subscribe()
    }
    
    func concatMapExample() -> Disposable {
        let numbers: Observable<Int> = Observable
            .interval(0.5, scheduler: MainScheduler.instance)
            .take(2)
        
        let combinedObservable = numbers.concatMap { value in
            return Observable<Int>
                .interval(0.3, scheduler: MainScheduler.instance)
                .take(3)
                .map { innerValue in
                    print("Outer Value \(value) Inner Value \(innerValue)")
            }
        }
        
        return combinedObservable.debug().subscribe()
    }
    
    func timeOut() -> Disposable {
        let numbers: Observable<Int> = Observable
            .interval(3.0, scheduler: MainScheduler.instance)
            .take(2)
        
        let quickNumbers: Observable<Int> = Observable
            .interval(0.5, scheduler: MainScheduler.instance)
            .take(2)
        
        return numbers
            .timeout(2.0, other: quickNumbers, scheduler: MainScheduler.instance)
            .debug()
            .subscribe()
    }
    
    func delaySubscription() -> Disposable {
        let numbers = Observable.of(1,2,3,4,5)
        return numbers.delaySubscription(3.0, scheduler: MainScheduler.instance).debug().subscribe()
    }
    
    func unwrap() -> Disposable {
        let maybeNumbers = Observable.of(1,2,nil,4,nil,6)
        
        return maybeNumbers
            .unwrap()
            .debug()
            .subscribe()
    }
    
    func skipUntil() -> Disposable {
        let trigger = Observable.just(2)
        
        let numbers: Observable<Int> = Observable
            .interval(0.3, scheduler: MainScheduler.instance)
            .take(10)
        
        return numbers
            .skipUntil(trigger.delaySubscription(2.0, scheduler: MainScheduler.instance))
            .debug()
            .subscribe()
    }
    
    func take() -> Disposable {
        let numbers = Observable.of(1,2,3,4,5,6,7)
        
        return numbers
            .take(3)
            .debug()
            .subscribe()
    }
    
    func takeWhile() -> Disposable {
        let numbers = Observable.of(1,2,3,4,nil,6,7)
        
        return numbers
            .takeWhile({ $0 != nil })
            .debug()
            .subscribe()
    }
    
    func takeUntil() -> Disposable {
        let numbers: Observable<Int> = Observable
            .interval(0.3, scheduler: MainScheduler.instance)
            .take(10)
        
        let trigger = Observable.just(10)
        
        return numbers
            .takeUntil(trigger.delay(1.5, scheduler: MainScheduler.instance))
            .debug()
            .subscribe()
    }
    
    func distinctUntilChanged() -> Disposable {
        let numbers = Observable.of(1,1,1,2,2,3,4,4,4,5,6,6,6,7)
    
        return numbers
            .distinctUntilChanged()
            .debug()
            .subscribe()
    }
    
    func throttle() -> Disposable {
        let numbers: Observable<Int> = Observable
            .interval(0.2, scheduler: MainScheduler.instance)
            .take(10)
        
        return numbers
            .throttle(0.5, scheduler: MainScheduler.instance)
            .debug()
            .subscribe()
    }
    
    func toArray() -> Disposable {
        let numbers: Observable<Int> = Observable
            .interval(0.3, scheduler: MainScheduler.instance)
            .take(10)
        
        return numbers
            .toArray()
            .debug()
            .subscribe()
    }
    
    func enumerated() -> Disposable {
        let numbers: Observable<Int> = Observable
            .interval(0.3, scheduler: MainScheduler.instance)
            .take(10)
        
        return numbers
            .enumerated()
            .map({ (index, element) in
                return index % 2 != 0 ? element : element * 2
            })
            .debug()
            .subscribe()
    }
    
    func catchError() -> Disposable {
        let numbers = BehaviorSubject<Int>(value: 0)
        
        let disposable = numbers
            .catchErrorJustReturn(-1)
            .debug()
            .subscribe()
        
        numbers.onNext(1)
        
        numbers.onNext(2)
        
        numbers.onError(ExampleErrors.sample)
        
        numbers.onCompleted()
        
        return disposable
    }
    
    func concat() -> Disposable {
        let numbers1 = Observable.of(2,3,4,5)
        let numbers2 = Observable.of(6,7,8,9)
        
        return numbers1
            .startWith(1)
            .concat(numbers2)
            .debug()
            .subscribe()
    }
    
    func merge() -> Disposable {
        let numbers1 = Observable.of(1,2,3,4,5)
        let numbers2 = Observable.of(6,7,8,9,10)
        
        return Observable.merge(numbers1, numbers2)
            .debug()
            .subscribe()
    }
    
    func combineLatest() -> Disposable {
        let numbers1 = Observable.of(1,2,3,4,5)
        let numbers2 = Observable.of(6,7,8,9,10)
        
        return Observable.combineLatest(numbers1, numbers2)
            .debug()
            .subscribe()
    }
    
    func reduce() -> Disposable {
        let numbers: Observable<Int> = Observable
            .interval(0.3, scheduler: MainScheduler.instance)
            .take(4)
        
        return numbers
            .reduce("", accumulator: { (last, new) in
                return last + String(new)
            })
            .debug()
            .subscribe()
    }
    
    func scan() -> Disposable {
        let numbers: Observable<Int> = Observable
            .interval(0.3, scheduler: MainScheduler.instance)
            .take(4)
        
        return numbers
            .scan("", accumulator: { (last, new) in
                return last + String(new)
            })
            .debug()
            .subscribe()
    }
    
    func buffer() -> Disposable {
        let incomingNumbers: Observable<Int> = Observable
            .interval(1.0, scheduler: MainScheduler.instance)
            .take(100)
        
        let bufferedNumbers = incomingNumbers.buffer(timeSpan: 2.0, count: 5, scheduler: MainScheduler.instance)
        
        return bufferedNumbers.debug().subscribe()
    }
    
    func scheduler() -> Disposable {
        let backgroundTask = Observable.just(1)
        let UITask = Observable.just(2)
        
        let globalScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
        
        return backgroundTask
            .observeOn(globalScheduler)
            .flatMap { _ in return UITask.subscribeOn(MainScheduler.instance) }
            .subscribe()
    }
}
