//
//  CustomOperatorExamples.swift
//  rxbasics
//
//  Created by Edem on 2018. 10. 04..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt
import SwiftyJSON

enum CustomExamples: String, CaseIterable, ExampleType {
    case extensions
    case operators
    
    var title: String {
        return rawValue.titleCased
    }
    
    var disposable: Disposable {
        let request = URLRequest(url: URL(string: "https://api.github.com/search/repositories?q=supercharge")!)

        switch self {
        case .extensions:
            return URLSession.shared.rx
                .response(request: request)
                .do(onNext: { print($0) })
                .cache()
                .subscribe()
        case .operators:
            return URLSession.shared.rx
                .json(request: request)
                .printJSON()
                .subscribe()
        }
    }
    
    var disposeHandler: DisposeHandler {
        return .disposeBag
    }
    
    var color: UIColor {
        return UIColor.Difficulty.medium
    }
}

// MARK: - Extensions
extension Reactive where Base: URLSession {
    func response(request: URLRequest) -> Observable<(HTTPURLResponse, Data)> {
        return Observable.create { observer in
            let task = self.base.dataTask(with: request) { (data, response, error) in
                guard let response = response, let data = data else {
                    observer.on(.error(ExampleErrors.sample))
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    observer.on(.error(ExampleErrors.sample))
                    return
                }
                
                observer.on(.next((httpResponse, data)))
                observer.on(.completed)
            }
            task.resume()
            return Disposables.create(with: task.cancel)
        }
    }
    
    func data(request: URLRequest) -> Observable<Data> {
        return response(request: request).map { (response, data) -> Data in
            if 200..<300 ~= response.statusCode {
                return data
            } else {
                throw ExampleErrors.sample
            }
        }
    }
    
    func string(request: URLRequest) -> Observable<String> {
        return data(request: request).map { data in
            return String(data: data, encoding: .utf8) ?? .init()
        }
    }
    
    func json(request: URLRequest) -> Observable<JSON> {
        return data(request: request).map { data in
            return try JSON(data: data)
        }
    }
    
    func image(request: URLRequest) -> Observable<UIImage> {
        return data(request: request).map { data in
            return UIImage(data: data) ?? .init()
        }
    }
}

// MARK: - Operators

fileprivate var internalCache = [String: Data]()

extension ObservableType where E == (HTTPURLResponse, Data) {
    func cache() -> Observable<E> {
        return self.do(onNext: { (response, data) in
            if let url = response.url?.absoluteString, 200..<300 ~= response.statusCode {
                internalCache[url] = data
            }
        })
    }
}

extension ObservableType where E == JSON {
    func printJSON() -> Observable<E> {
        return self.do(onNext: { print($0) })
    }
}
