//
//  DisposeHandler.swift
//  rxbasics
//
//  Created by Nagy Ádám on 2018. 08. 07..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import RxSwift

enum DisposeHandler {
    case disposeBag
    case disposeManually
    case doNotDispose
}
