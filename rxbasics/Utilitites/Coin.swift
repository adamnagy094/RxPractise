//
//  Coin.swift
//  rxbasics
//
//  Created by Nagy Ádám on 2018. 08. 07..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import Foundation

enum Coin: Int {
    case head
    case tail

    static var rand: Coin {
        return Coin(rawValue: Int(arc4random_uniform(2)))!
    }
}
