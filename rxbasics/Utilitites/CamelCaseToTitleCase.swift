//
//  CamelCaseToTitleCase.swift
//  rxbasics
//
//  Created by Nagy Ádám on 2018. 08. 07..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import Foundation

extension String {
    var titleCased: String {
        return unicodeScalars.reduce("") {
            if CharacterSet.uppercaseLetters.contains($1) == true {
                return ($0 + " " + String($1).lowercased())
            } else {
                return $0 + String($1)
            }
        }
    }
}
