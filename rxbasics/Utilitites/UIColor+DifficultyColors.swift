//
//  UIColor+DifficultyColors.swift
//  rxbasics
//
//  Created by Edem on 2018. 10. 03..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import UIKit

extension UIColor {
    enum Difficulty {
        static let easy: UIColor = UIColor(red: 126/255, green: 248/255, blue: 120/255, alpha: 0.5)
        static let medium: UIColor = UIColor(red: 248/255, green: 219/255, blue: 120/255, alpha: 1.0)
        static let hard: UIColor = UIColor.red
    }
}
