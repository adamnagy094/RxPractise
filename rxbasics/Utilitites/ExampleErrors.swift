//
//  ExampleErrors.swift
//  rxbasics
//
//  Created by Edem on 2018. 10. 04..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import Foundation

enum ExampleErrors: Error {
    case sample
}
