//
//  DataHandler.swift
//  rxbasics
//
//  Created by Nagy Ádám on 2018. 08. 06..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import RxSwift

typealias Section = (title: String, practises: [ExampleType])

class DataHandler {
    let bag = DisposeBag()

    let datas: [Section] = [
        ("Advanced Operators", AdvancedOperatorExamples.allCases),
        ("Create Custom Stuff", CustomExamples.allCases),
        ("Observers - Observables", ObservableExamples.allCases),
        ("Memory management 🚰", DisposingAndTerminatingExamples.allCases),
        ("Subjects", SubjectsExamples.allCases),
        ("Traits", TraitsExamples.allCases),
        ("Operators", OperatorExamples.allCases)
    ]

    func call(test: ExampleType) {
        example(of: test.title) {
            switch test.disposeHandler {
            case .disposeBag:
                let disposable = test.disposable
                disposable.disposed(by: bag)
            case .disposeManually:
                let disposable = test.disposable
                disposable.dispose()
            case .doNotDispose:
                _ = test.disposable
            }
        }
    }
}
